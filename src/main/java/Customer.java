import java.util.ArrayList;
import java.util.List;

class Customer {
    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        this.rentals.add(arg);
    }

    private String getName() {
        return name;
    }

    public String statement() {
        StringBuilder result = new StringBuilder(String.format("Rental Record for %s\n", getName()));
        for (Rental rental : this.rentals)
            result.append(String.format("\t%s\t%.1f\n", rental.getMovieTitle(), rental.getCharge()));
        result.append(String.format("You owed %.1f\n", getTotalAmount()));
        result.append(String.format("You earned %d frequent renter points\n", getFrequentRenterPoints()));

        return result.toString();
    }

    private double getTotalAmount() {
        double totalAmount = 0;
        for (Rental rental : this.rentals) {
            totalAmount += rental.getCharge();
        }
        return totalAmount;
    }

    private int getFrequentRenterPoints() {
        return this.rentals.stream().mapToInt(Rental::getFrequentRenterPoints).sum();
    }

}
