public abstract class Movie {
    private String title;

    Movie(String title) {
        this.title = title;
    }

    String getTitle() {
        return title;
    }

    abstract double getCharge(int daysRented);

    abstract int getFrequentRenterPoints(int daysRented);
}

