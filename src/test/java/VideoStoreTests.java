import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VideoStoreTests {

    @Test
    public void singleNewReleaseStatement() {
    	Customer customer = new Customer("John");

        customer.addRental(new Rental(getNewRelease("The Cell"), 3));

    	assertEquals("Rental Record for John\n\tThe Cell\t9.0\nYou owed 9.0\nYou earned 2 frequent renter points\n", customer.statement());
    }


    @Test
    public void dualNewReleaseStatement() {
        Customer customer = new Customer("John");
        customer.addRental(new Rental(getNewRelease("The Cell"), 3));
        customer.addRental(new Rental(getNewRelease("The Trigger Movie"), 3));

        assertEquals("Rental Record for John\n\tThe Cell\t9.0\n\tThe Trigger Movie\t9.0\nYou owed 18.0\nYou earned 4 frequent renter points\n", customer.statement());
    }

    @Test
    public void singleChildrenStatement() {
        Customer customer = new Customer("John");

        customer.addRental(new Rental(getChildrenMovie("The Trigger Movie"), 3));

        assertEquals("Rental Record for John\n\tThe Trigger Movie\t1.5\nYou owed 1.5\nYou earned 1 frequent renter points\n", customer.statement());
    }

    @Test
    public void singleChildrenFor4DaysStatement() {
        Customer customer = new Customer("John");

        customer.addRental(new Rental(getChildrenMovie("The Trigger Movie"), 4));

        assertEquals("Rental Record for John\n\tThe Trigger Movie\t3.0\nYou owed 3.0\nYou earned 1 frequent renter points\n", customer.statement());
    }

    @Test
    public void multipleRegularStatement() {
        Customer customer = new Customer("John");

        customer.addRental(new Rental(createRegularMovie("Wall-e"),1));
        customer.addRental(new Rental(createRegularMovie("8 1/2"), 2));
        customer.addRental(new Rental(createRegularMovie("The Cell"), 3));

        assertEquals("Rental Record for John\n\tWall-e\t2.0\n\t8 1/2\t2.0\n\tThe Cell\t3.5\nYou owed 7.5\nYou earned 3 frequent renter points\n", customer.statement());
    }

    private Movie getNewRelease(String title) {
        return new NewReleaseMovie(title);
    }

    private Movie getChildrenMovie(String title) {
        return new ChildrenMovie(title);
    }

    private Movie createRegularMovie(String title) {
        return new RegularMovie(title);
    }


}
